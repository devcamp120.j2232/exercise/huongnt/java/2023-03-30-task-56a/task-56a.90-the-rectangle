package com.dev.rectanglerestapi;

public class Rectangle {
    private float length = 1.0f;
    private float width = 1.0f;

    //khởi tạo phương thức
    public Rectangle() {
    }

    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }

    //getter & setter
    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    //phương thức khác
    public double getArea(){
        return this.length*this.width;

    }

    public double getPerimeter(){
        return 2*(this.length+this.width);
    }

    //in ra console
    @Override
    public String toString() {
        return "Rectangle [length=" + length + ", width=" + width + "]";
    }

    
    
    
}
