package com.dev.rectanglerestapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.service.annotation.PutExchange;

import com.dev.rectanglerestapi.Rectangle;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class RectangleControll {
    @GetMapping("rectangle-area")
    public double getRectangleArea(@RequestParam float length, float width){

        Rectangle rectangle1 = new Rectangle(length, width);
        return rectangle1.getArea();
    }
    


    @GetMapping("rectangle-perimeter")
    public double getRectanglePerimeter(@RequestParam float length, float width){

        Rectangle rectangle1 = new Rectangle(length, width);
        return rectangle1.getPerimeter();
    }
    
}
